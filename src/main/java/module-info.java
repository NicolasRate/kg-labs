module Application {
    requires javafx.controls;
    requires javafx.fxml;

    opens com.nrate.kglab4 to javafx.fxml;
    exports com.nrate.kglab4;
    exports com.nrate.kglab4.controllers;
    opens com.nrate.kglab4.controllers to javafx.fxml;
}