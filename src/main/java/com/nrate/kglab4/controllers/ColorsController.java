package com.nrate.kglab4.controllers;

import com.nrate.kglab4.App;
import javafx.fxml.FXML;

import java.io.IOException;


public class ColorsController {

    @FXML
    private void switchToFractal() throws IOException {
        App.setRoot("fractal");
    }

    @FXML
    private void switchToTriangle() throws IOException {
        App.setRoot("triangle");
    }
}
