package com.nrate.kglab4.controllers;

import com.nrate.kglab4.App;
import com.nrate.kglab4.Fractal;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.ColorPicker;
import javafx.scene.control.Slider;
import javafx.scene.control.Spinner;
import javafx.scene.control.TextField;
import javafx.scene.paint.Color;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import static java.lang.Math.*;

public class FractalController implements Initializable {

    private static final int CANVAS_WIDTH = 487;
    private static final int CANVAS_HEIGHT = 447;

    private static int counter = 10;

    @FXML
    public Canvas canvas;

//    @FXML
//    public TextField cRe;

    @FXML
    public TextField cIm;

    @FXML
    public Spinner<Integer> cBm;
//    @FXML
//    public TextField cBm;

    @FXML
    public Slider stepSlider;

    @FXML
    public ColorPicker fractalColorPicker;


    private Fractal fractal = new Fractal();

    private void paintSet(GraphicsContext ctx, Fractal fractal) {

        Color[] colors = new Color[counter];
//        int[] colors= new int[10];
        for (int i = 0; i < counter; i++) {
            colors[i] = Color.rgb((int) (fractal.getColorSchema().getRed() * 255 * i / (double) counter), (int) (fractal.getColorSchema().getGreen() * 255 * i / (double) counter), (int) (fractal.getColorSchema().getBlue() * 255 * i / (double) counter));
//            System.out.printf((int)(fractal.getColorSchema().getRed() * 255) + " " + (int)(fractal.getColorSchema().getGreen() * 255) + " " + (int)(fractal.getColorSchema().getBlue() * 255) + System.lineSeparator());
//            colors[i] = Color.rgb(fractal.getColorSchema().getRed() + i*2, fractal.getColorSchema().getGreen() + i*2,fractal.getColorSchema().getBlue() + i*2);
//            colors[i] = Color.rgb(i/256f, 1, i/(i+8f));
//            colors[i] = Color.rgb(i+50, 1, i * 10);
        }

        for (int row = 0; row < CANVAS_HEIGHT; row++) {
            for (int col = 0; col < CANVAS_WIDTH; col++) {
                double x = (col - CANVAS_WIDTH / fractal.getXLimit()) * fractal.getYLimit() / CANVAS_WIDTH;
                double y = (row - CANVAS_HEIGHT / fractal.getXLimit()) * fractal.getYLimit() / CANVAS_WIDTH;
                int iteration = 0;
                while (abs(x + y) < 16 && iteration < counter + 1) {
                    double x_new = (x * sin(x) * cosh(y) - y * cos(x) * sinh(y));
                    y = (y * sin(x) * cosh(y) + x * cos(x) * sinh(y));
                    x = x_new;
                    iteration++;
                }
                if (iteration < counter) {
                    ctx.setFill(colors[iteration]);
                    ctx.fillRect(col, row, 1, 1);
                } else {
                    ctx.setFill(Color.BLACK);
                    ctx.fillRect(col, row, 1, 1);
                }
            }
        }
        // 2 5
//        double precision = Math.max((fractal.getReMax() - fractal.getReMin()) / CANVAS_WIDTH, (fractal.getImMax() - fractal.getImMin()) / CANVAS_HEIGHT);

//        double convergenceValue;
//        int fractalConvergenceSteps = fractal.getConvergenceSteps();
//        for (double c = fractal.getReMin(), xR = 0; xR < CANVAS_WIDTH; c = c + precision, xR++) {
//            for (double ci = fractal.getImMin(), yR = 0; yR < CANVAS_HEIGHT; ci = ci + precision, yR++) {
//
//                if (fractal.isIsMandelbrot()) {
//                    convergenceValue = FractalBean.checkConvergence(ci, c, 0, 0, fractalConvergenceSteps);
//                } else {
//                    convergenceValue = FractalBean.checkConvergence(fractal.getZi(), fractal.getZ(), ci, c, fractalConvergenceSteps);
//                }
//                double t = convergenceValue / fractalConvergenceSteps;
//
//                ctx.setFill(convergenceValue != fractalConvergenceSteps ? fractal.getColorSchema(t) : fractal.getConvergenceColor());
//                ctx.fillRect(xR, yR, 1, 1);
//            }
//        }
    }


    @FXML
    private synchronized void draw() {
        paintSet(canvas.getGraphicsContext2D(), fractal);
    }

    @FXML
    private void switchToColors() throws IOException {
        App.setRoot("colors");
    }

    @FXML
    private void switchToTriangle() throws IOException {
        App.setRoot("triangle");
    }

//    @FXML
//    private synchronized void moveFractal(KeyEvent ke) {
//        switch (ke.getCode().toString()) {
//            case "D": {
////                fractal.moveRight(0.2);
//                break;
//            }
//            case "A": {
////                fractal.moveRight(-0.2);
//                break;
//            }
//            case "S": {
////                fractal.moveUp(0.2);
//                break;
//            }
//            case "W": {
////                fractal.moveUp(-0.2);
//                break;
//            }
//            case "ADD": {
////                fractal.zoom(1 / 1.5);
//                break;
//            }
//            case "SUBTRACT": {
////                fractal.zoom(1.5);
//                break;
//            }
//            default: {
//                return;
//            }
//        }
//        draw();
//    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
//        cRe.textProperty().addListener((observable, oldValue, newValue) -> {
//            if (newValue.length() != 0)
//                fractal.setxLimit(Double.parseDouble(newValue));
//            fractal.setxLimit(newValue.length() == 0 ?  Double.parseDouble(newValue));
//            draw();
//        });
        cIm.textProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue.length() != 0)
                fractal.setyLimit(Double.parseDouble(newValue));
            draw();
        });
//        cBm.textProperty().addListener((observable, oldValue, newValue) -> {
//            if (newValue.length() != 0)
//                fractal.setIterationsNumber(Integer.parseInt(newValue));
//            counter = Integer.parseInt(newValue);
//            draw();
//        });
//        stepSlider.valueProperty().addListener((observable, oldValue, newValue) -> {
////            fractal.setConvergenceSteps(newValue.intValue());
//            draw();
//        });
        fractalColorPicker.setOnAction(e -> {
            fractal.setColorSchema(fractalColorPicker.getValue());
            draw();
        });
        draw();
    }

    //
//    @FXML
//    private synchronized void newMethod() {
//        fractal.setxLimit(Double.parseDouble(cRe.textProperty().getValue()));
//        fractal.setyLimit(Double.parseDouble(cIm.textProperty().getValue()));
//        fractal.setColorSchema(fractalColorPicker.getValue());
//        draw();
//    }

}