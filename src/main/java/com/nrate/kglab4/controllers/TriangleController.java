package com.nrate.kglab4.controllers;

import com.nrate.kglab4.App;
import javafx.fxml.FXML;

import java.io.IOException;


public class TriangleController {

    @FXML
    private void switchToFractal() throws IOException {
        App.setRoot("fractal");
    }

    @FXML
    private void switchToColors() throws IOException {
        App.setRoot("colors");
    }
}
