package com.nrate.kglab4;

import javafx.scene.paint.Color;

public class Fractal {
    private int iterationsNumber;
    private double xLimit;
    private double yLimit;
    private Color colorSchema = Color.BISQUE;
    private Color convergenceColor = Color.BLACK;

    public void setIterationsNumber(int iterationsNumber) {
        this.iterationsNumber = iterationsNumber;
    }

    public double getxLimit() {
        return xLimit;
    }

    public void setxLimit(double xLimit) {
        this.xLimit = xLimit;
    }

    public double getyLimit() {
        return yLimit;
    }

    public void setyLimit(double yLimit) {
        this.yLimit = yLimit;
    }

    public void setColorSchema(Color colorSchema) {
        this.colorSchema = colorSchema;
    }

    public Color getConvergenceColor() {
        return convergenceColor;
    }

    public void setConvergenceColor(Color convergenceColor) {
        this.convergenceColor = convergenceColor;
    }

    public Fractal(){
        iterationsNumber = 10;
        xLimit = 2;
        yLimit = 7.5;
    }

    public Fractal(int i, double x, double y){
        iterationsNumber = i;
        xLimit = x;
        yLimit = y;
    }

    public int getIterationsNumber() {
        return iterationsNumber;
    }

    public double getXLimit() {
        return xLimit;
    }

    public double getYLimit() {
        return yLimit;
    }

    public Color getColorSchema(double t) {
        return Color.hsb(colorSchema.getHue(), colorSchema.getSaturation(), colorSchema.getBrightness());
    }
    public Color getColorSchema() {
        return colorSchema;
    }
}
